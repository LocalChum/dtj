package com.localchum.digitechjava;

import java.util.Scanner;

/**
 * Created by localchum on 10/02/16.
 */
public class L09EvenOrOdd {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        System.out.println("Enter any non-integer to exit.");
        try {
            do {
                System.out.print("Enter an integer: ");
                int val = s.nextInt();
                System.out.format("The integer %d is %s.\n", val, val % 2 == 0 ? "even" : "odd");
            } while (true);
        } catch (Exception ignored) { }
    }

}
