package com.localchum.digitechjava;

/**
 * Created by localchum on 10/02/16.
 */
public class L14Speaking {

    public static void main(String[] args) {
        System.out.println("Decimal\tBinary\tOctal\tHex\tCharacter");
        for (int i = 65; i <= 90; i++) {
            System.out.format("%d\t%s\t%s\t%s\t%s\n", i, Integer.toBinaryString(i), Integer.toOctalString(i), Integer.toHexString(i), (char) i);
        }
    }

}
