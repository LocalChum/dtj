package com.localchum.digitechjava;

import java.util.Scanner;

/**
 * Created by localchum on 10/02/16.
 */
public class L07BName {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("What is your first name? ");
        String f = s.next();
        System.out.print("What is your last name? ");
        System.out.format("Your full name is %s %s.\n", f, s.next());
    }

}
