package com.localchum.digitechjava;

import java.util.Scanner;

/**
 * Created by localchum on 10/02/16.
 */
public class L15BOverdrawn {

    private static class Account {
        private final String name;
        private double balance;

        public Account(String name, double balance) {
            this.name = name;
            this.balance = balance;
        }

        public void deposit(double amt) {
            balance += amt;
        }

        public void withdraw(double amt) {
            balance -= amt;
        }

        public String getName() {
            return name;
        }

        public double getBalance() {
            return balance;
        }
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Name: ");
        String name = s.nextLine();
        System.out.print("Balance: ");
        double bal = s.nextDouble();

        Account acc = new Account(name, bal);
        acc.deposit(505.22);
        System.out.format("%.2f\n", acc.getBalance());
        acc.withdraw(100);

        System.out.format("The %s account balance is, %.2f", acc.getName(), acc.getBalance());
    }

}
