package com.localchum.digitechjava;

/**
 * Created by localchum on 10/02/16.
 */
public class L15ADiameter {

    private static class Circle {
        private final double radius;

        public Circle(double radius) {
            this.radius = radius;
        }

        public double diameter() {
            return radius * 2;
        }
    }

    public static void main(String[] args) {
        System.out.println(new Circle(35.5).diameter());
    }

}
