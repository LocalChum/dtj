package com.localchum.digitechjava;

/**
 * Created by localchum on 10/02/16.
 */
public class L01FromMeToYou {

    public static void main(String[] args) {
        System.out.println("From: Bill Smith\n" +
                "Address: Dell Computer, Bldg 13\n" +
                "Date: April 12, 2005\n" +
                "To: Jack Jones\n" +
                "Message: Help! I'm trapped inside a computer!");
    }

}
