package com.localchum.digitechjava;

import java.util.Scanner;

/**
 * Created by localchum on 10/02/16.
 */
public class L11Reverse {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Please enter your name. ");
        String name = s.nextLine();

        for (int i = name.length() - 1; i >= 0; i--) {
            System.out.print(name.substring(i, i + 1).toLowerCase());
        }
    }

}
