package com.localchum.digitechjava;

import java.util.Scanner;

/**
 * Created by localchum on 10/02/16.
 */
public class L07ARadiusOfCircle {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("What is the area? ");
        double area = s.nextDouble();
        System.out.format("Radius of your circle is %f", Math.sqrt(area / Math.PI));
    }

}
