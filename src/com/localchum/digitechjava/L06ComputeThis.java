package com.localchum.digitechjava;

/**
 * Created by localchum on 10/02/16.
 */
public class L06ComputeThis {

    public static void main(String[] args) {
        System.out.println(3 * Math.PI * Math.sin(Math.toRadians(187)) + Math.abs(Math.cos(Math.toRadians(122))));
        System.out.println(Math.pow(14.72, 3.801) + Math.log(72));
    }

}
