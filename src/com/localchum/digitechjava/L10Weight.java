package com.localchum.digitechjava;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by localchum on 10/02/16.
 */
public class L10Weight {

    public static final Map<String, Double> conversion = new LinkedHashMap<>();

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        System.out.print("What is your weight on Earth? ");
        double weight = s.nextDouble();

        int i = 0;
        for (String planet : conversion.keySet()) {
            System.out.format("%d. %s\n", ++i, planet);
        }

        System.out.print("Selection? ");
        int sel = s.nextInt();

        double modifier = 0;
        String name = null;

        Iterator<Map.Entry<String, Double>> it = conversion.entrySet().iterator();
        int j = 1;
        while (it.hasNext()) {
            Map.Entry<String, Double> e = it.next();
            if (j++ == sel) {
                modifier = e.getValue();
                name = e.getKey();
            }
        }

        if (name != null) {
            System.out.format("Your weight on %s would be %f\n", name, weight * modifier);
        } else {
            System.out.println("Unknown planet");
        }
    }

    static {
        conversion.put("Voltar", 0.091);
        conversion.put("Krypton", 0.720);
        conversion.put("Fertos", 0.865);
        conversion.put("Servontos", 4.612);
    }

}
